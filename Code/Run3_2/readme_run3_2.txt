Run 3_2 Convolution Neural Network

This folder consists of two Matlab programs and run3_2 classified test data text file.

For this program to work make sure that Deep Learning Toolbox is installed in Matlab.

Matlab files:

1)Run3_CNN
2)Run3_CNN_second_test

To run the program with the test data please run 'Run3_CNN_second_test'.
Change directories for the training and test folders in lines 4 and 46.
Also, change directory to save the run3_2.txt file in line 50.

To check the approximate percentage on the training data itself please run the program 'Run3_CNN'.
Change directories for the training folder in line 5.

To load different pretrained networks simply uncomment the one you wish to use, in lines 9 to 11,
and uncomment the corresponding feature layer between lines 30 and 32.