clc;
clear all;
close all;
setDir  = fullfile('C:/Users/Sandeep/Downloads/training/training')
imagedstore = imageDatastore(setDir,'IncludeSubfolders',true,'LabelSource', 'foldernames')
table = countEachLabel(imagedstore)

% Load pre-trained network (uncomment only one)
%network = alexnet();
network = resnet50();
%network = inceptionresnetv2();

% Pre-process images to network dimension requirements
imageSize = network.Layers(1).InputSize;
ResizedTrainingSet = augmentedImageDatastore(imageSize, imagedstore, 'ColorPreprocessing', 'gray2rgb');

% Get network weights for second convolution layer
w_1 = network.Layers(2).Weights;
% Scale and resize the weights for visualisation
w_1 = mat2gray(w_1);
w_1 = imresize(w_1,5);
% Display montage of network weights
figure
montage(w_1)
title('First convolution layer weights')

% Extract training features from layer before classification layer
% Uncomment only one featureLayer corresponding to chosen pre-trained network

%featureLayer = 'fc8'; % alexnet
featureLayer = 'fc1000'; % resnet50
%featureLayer = 'predictions'; % inceptionresnetv2

trainingFeatures = activations(network, ResizedTrainingSet, featureLayer, ...
    'MiniBatchSize', 32, 'OutputAs', 'columns');
% Get training labels from the trainingSet
trainingLabels = imagedstore.Labels;
% Train multiclass SVM classifier using Stochastic Gradient Descent
classifier = fitcecoc(trainingFeatures, trainingLabels, ...
    'Learners', 'Linear', 'Coding', 'onevsall', 'ObservationsIn', 'columns');
% Extract test features using CNN
%testFeatures = activations(network, ResizedTestSet, featureLayer, ...
 %   'MiniBatchSize', 32, 'OutputAs', 'columns');

% Pass CNN image features to trained classifier
testDir  = fullfile('C:/Users/Sandeep/Downloads/testing/testing');
testSet = imageDatastore(testDir);
[n p]=size(testSet.Files);

fileID = fopen('C:/Users/Sandeep/Desktop/run3_2.txt','w');
for i = 1:n
    index=string(testSet.Files(i));
    [path,name,ext] = fileparts(index);
    newImage=readimage(testSet,i);
    dataStore = augmentedImageDatastore(imageSize, newImage, 'ColorPreprocessing', 'gray2rgb');
    % Extract image features using CNN
    imageFeatures = activations(network, dataStore, featureLayer, 'OutputAs', 'columns');
    % Predict image class using the classifier
    label = string(predict(classifier, imageFeatures, 'ObservationsIn', 'columns'));
    fprintf(fileID,'%s.jpg %s\r\n',name,label);
end
fclose(fileID);
