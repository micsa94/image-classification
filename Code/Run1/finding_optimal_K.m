%This is the test file to check what is the approximate efficiency percentages

clc;
clear all;
close all;
training_data=[];
testing_data=[];

str={'bedroom' 'Coast' 'Forest' 'Highway' 'industrial' 'Insidecity' 'kitchen' 'livingroom' 'Mountain' 'Office' 'OpenCountry' 'store' 'Street' 'Suburb' 'TallBuilding'};
index   = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14,15];
chosen  = str(index);

%In this part we are seperating the training data into 90% training / 10% testing
for j = 1:15
    addpath(sprintf('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/training/training/%s',chosen{j}));
    for i=0:89
        training=imread(sprintf('%d.jpg',i));
        vector = preprocess(training);
        training_data = [training_data;vector];
    end
end

for j = 1:15
    addpath(sprintf('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/training/training/%s',chosen{j}));
    for i=90:99
        testing=imread(sprintf('%d.jpg',i));
        vector = preprocess(testing);
        testing_data = [testing_data;vector];
    end
end

perc=[];
optimal_k=[];

%Taking K from 1 to 100 the find the optimal value of K
for k=1:100
    idx = knnsearch(training_data,testing_data,'K',k);
    classification={};
    check={};
    
    for i = 1:150
        imageindex = i-1;
        class = (mode(ceil(idx(i,:) / 100)));
        c=str(class);
        check=[check,class];
        classification=[classification,c];
    end
    
    classification=transpose(classification);
    check=transpose(check);
    ratio=0;
    check=cell2mat(check);
    a=[0,10,20,30,40,50,60,70,80,90,100,110,120,130,140];
    
    for j=1:15
        for i=1:10
            if check(a(j)+i)==index(j)
                ratio=ratio+1;
            end
        end
    end
    
    perc=[perc,ratio];
    optimal_k=[optimal_k,k];
end

%Plotting K values by the approximate efficiency percentages
optimal_k=transpose(optimal_k);
perc=transpose((perc)/150)*100;
figure(); plot(optimal_k,perc)
title('Finding Optimal K with 16x16 Images')
ylabel('Percentage') 
xlabel('K Values') 