Run 1 using K-Nearest-Neighbours

This folder consists of three Matlab programs and Run1 classified test data text file.

Matlab files:

1)run1
2)preprocess
3)finding_optimal_K

To run the program with the test data please run 'run1'.
Change directories for the training and test folders in lines 14 and 23.
Also, change directory to save the run1.txt file in line 37.

To check the approximate percentage vs K on the training data itself please run the program 'finding_optimal_K'.
Change directories for the training and test folders in lines 15 and 24.