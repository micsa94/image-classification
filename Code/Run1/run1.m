clc;
clear all;
close all;
training_data=[];
testing_data=[];

%Defining the classes and index to each class between 1 to 15
str={'bedroom' 'Coast' 'Forest' 'Highway' 'industrial' 'Insidecity' 'kitchen' 'livingroom' 'Mountain' 'Office' 'OpenCountry' 'store' 'Street' 'Suburb' 'TallBuilding'};
index   = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14,15];
chosen  = str(index);

%Gathering all training images
for j = 1:15
    addpath(sprintf('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/training/training/%s',chosen{j}));
    for i=0:99
        training=imread(sprintf('%d.jpg',i));
        vector = preprocess(training);
        training_data = [training_data;vector];
    end
end

%Gathering All testing images
testDir  = fullfile('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/testing/testing');
testSet = imageDatastore(testDir);
[n p]=size(testSet.Files);
for i = 1:n
    test=readimage(testSet,i);
    vector = preprocess(test);
    testing_data = [testing_data;vector];
end

%Performing K Nearest Neighbours to define classes for testing data
idx = knnsearch(training_data,testing_data,'K',6);

%Identifying classes with respect to the KNNs found and printing the
%answers in a run1.txt
fileID = fopen('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/Run1/run1.txt','w');
for i=1:n
    index=string(testSet.Files(i));
    [path,name,ext] = fileparts(index);
    class = (mode(ceil(idx(i,:) / 100)));
    c=string(str(class));
    fprintf(fileID,'%s.jpg %s\r\n',name,c);
end
fclose(fileID);

