%In this function we are checking the size of the input images
function vector = preprocess(img1)
img_1=im2single(img1);
[x,y]=size(img_1);

%if the image is not square crop the image
if (y>x)
    diff=y-x;
    img_1 = img_1(1:end,(diff/2)+1:end-(diff/2));
    [x,y]=size(img_1);
end

if (x>y)
    diff=x-y;
    img_1 = img_1((diff/2)+1:end-(diff/2),1:end);
    [x,y]=size(img_1);
end

%Once the image is a square, resize to 16x16
if (x==y)
    img = imresize(img_1,[16 16]);
end

%get the image into a vector of 1x256, performing zero mean and unit length
vector = transpose(img(:));
vector=vector-mean(vector);
vector=vector/norm(vector);
%norm(vector) this should be 1