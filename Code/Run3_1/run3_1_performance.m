%This is a performance program to see the approximate efficiency percentage
%of 10% of the training data set as test data

clc;
clear all;
close all;

%Gathering the training data from all folders with their respective labels
trainDir  = fullfile('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/training/training');
data = imageDatastore(trainDir,'IncludeSubfolders',true,'LabelSource','foldernames');

%Splitting the training data into 90% training data, 10% testing data
[testSet,trainingSet] = splitEachLabel(data,0.1,'randomize');

%Gathering the features and using K-means to cluster the features.
bag_of_words =  bagOfFeatures(trainingSet);

%Training the data using histograms of visual words.
categoryClassifier = trainImageCategoryClassifier(trainingSet,bag_of_words);

%Evaluating the test data to find an approximate efficiency percentage
percentage = evaluate(categoryClassifier,testSet);
mean(diag(percentage))

% Plot the histogram of visual word occurrences for training image 1
img = readimage(data, 1);
histogram_img_1 = encode(bag_of_words, img);

figure(1);bar(histogram_img_1)
title('Visual Word Index vs Occurrence')
xlabel('Visual Word Index')
ylabel('Occurrence of Visual Word')