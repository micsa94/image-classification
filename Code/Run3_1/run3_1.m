clc;
clear all;
close all;

%Gathering all training data from all folders including folder labels
trainDir  = fullfile('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/training/training');
trainingSet = imageDatastore(trainDir,'IncludeSubfolders',true,'LabelSource','foldernames');

%Performing feature extractor SURF using the grid method, with a grid step 
%of 8x8, the defining the bag of words by clustering the data using K-means with 500 clusters, checking 
%which class has the least strongest features. Once we have that
%value we take that amount of strongest features from each category, and then
%finding the center for each cluster, which is equivalent to a visual word
bag_of_words =  bagOfFeatures(trainingSet);

%Training the data on a multiclass classifier which uses the binary Support
%Vector Machine. This classifier uses the visual words obtained by the
%bagOfFeatures function to get a histogram of visual words, which is being
%used to train the classifier.
categoryClassifier = trainImageCategoryClassifier(trainingSet,bag_of_words);

%Gathering all testing data
testDir  = fullfile('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/testing/testing');
testSet = imageDatastore(testDir);
[n p]=size(testSet.Files);

%Predicting the class for every test image and printing the output in text file run3_1.txt
fileID = fopen('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_3/Run3/run3_1.txt','w');
for i = 1:n
    index=string(testSet.Files(i));
    [path,name,ext] = fileparts(index);
    test=readimage(testSet,i);
    [labelIdx, score] = predict(categoryClassifier,test);
    class=string(categoryClassifier.Labels(labelIdx));
    fprintf(fileID,'%s.jpg %s\r\n',name,class);
end
fclose(fileID);