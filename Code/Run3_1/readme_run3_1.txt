Run 3_1 Bag of Words, SURF, K-Means and Linear SVM

This folder consists of two Matlab programs and run3_1 classified test data text file.

For this program to work make sure that Computer Vision System Toolbox is installed in Matlab.

Matlab files:

1)run3_1
2)run3_1_performance

To run the program with the test data please run 'run3_1'.
Change directories for the training and test folders in lines 6 and 23.
Also, change directory to save the run3_1.txt file in line 28.

To check the approximate percentage on the training data itself please run the program 'run3_1_performance'.
Change directories for the training folder in line 9.