The flow of run2 can be configured using the variables at the start of the program

boolean test_unlabeled_data: test the files in train_root_folder ('training') and produce 
	a txt file by the name held in variable output_file. Ensure training_images_per_class
	holds the number of images per class.

boolean resize_images: resize the images for better performance, good for debugging, 
	resize factor held in variable resize_scale.

boolean test_labeled_data: test data in the labelled folders to get a sense of
	accuracy of the model. If you do this, indicate the root folder for training with
	labeled_test_root_folder, which contains images in directories like in the 'training' folder. 
	You must also specify how many images there are for each classes in the in this case with variable training_images_per_class. There is a quirk
	that you should set training_images_per_class to 100 if you are testing the unlabeled
	images, when you do this and also test_unlabeled_data you will get an accuracy that
	indicates the model is very accurate but this is because you are testing with data that 
	was fed into the model (so set to 90 if you have taken 10 images out of each class of training
	and put them into labeled test directory under the same folders) 
	



