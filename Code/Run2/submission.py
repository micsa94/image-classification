#!/usr/bin/env python
# coding: utf-8
import math
import os
import random

import matplotlib.image as mpimg
import numpy as np
from skimage.transform import resize
from skimage.util.shape import view_as_windows
from sklearn.cluster import MiniBatchKMeans
from sklearn.svm import LinearSVC

# variables place here for configurability
train_root_folder = 'training'
labeled_test_root_folder = 'labeledtest'
unlabeled_test_root_folder = 'testing'
output_file = "run2.txt"

# to test on the full set of test data, produce run2.txt
test_unlabeled_data = True

num_clusters = 600
batch_size = 1000

# assumes equal number of training images per class
training_images_per_class = 100

# resize the images to see the affects of features loss
resize_images = False
resize_scale = 64

# dimensions for an x*x window for sample extraction, and sample rate/separtion
window_shape_length = 8
sample_seperation = 4

# the number of samples taken from each image for training and labelling
sample_number = 1000

# to test on labeled data to get a sense of accuracy of the model learned
test_labeled_data = False

# assumes each folder is a class
folders = ('bedroom', 'Coast', 'Forest', 'Highway', 'industrial', 'Insidecity', 'kitchen', 'livingroom',
          'Mountain', 'Office', 'OpenCountry', 'store', 'Street', 'Suburb', 'TallBuilding')

classes = ('bedroom', 'coast', 'forest', 'highway', 'industrial', 'insidecity', 'kitchen', 'livingroom',
          'mountain', 'office', 'openCountry', 'store', 'street', 'suburb', 'tallBuilding')

test_images = ('bedroom', 'Coast', 'Forest', 'Highway', 'industrial', 'Insidecity', 'kitchen', 'livingroom',
           'Mountain', 'Office', 'OpenCountry', 'store', 'Street', 'Suburb', 'TallBuilding')

# return jpg images from the specified path
def load_images_from_folder(folder):
    images = []
    for file_name in os.listdir(folder):
        if file_name.endswith(".jpg"):
            img = mpimg.imread(os.path.join(folder, file_name))
            if img is not None:
                if(resize_images == True):
                    img = resize(img, (resize_scale, resize_scale), anti_aliasing=True)
                images.append(img)

    return images

# given images and sample seperation and window shape, return
def get_normalised_patches(images, sample_seperation, window_shape, sample_number):
    normalised_patches = np.empty((0, window_shape[0]*window_shape[1]), int)

    for i in range(images.shape[0]):
        patches = view_as_windows(images[i], window_shape)

        feature_patches = patches[0][::sample_seperation]
        for i in range(1, math.floor(patches.shape[0] / sample_seperation)):
            feature_patches = np.append(feature_patches, patches[i * sample_seperation][::sample_seperation], axis=0)

        # select random samples of the feature patches extracted
        sampled_patches = [feature_patches[random.randint(0, feature_patches.shape[0] - 1)]]
        for x in range(sample_number - 1):
            sampled_patches = np.append(sampled_patches, [feature_patches[random.randint(0, feature_patches.shape[0] - 1)]], axis=0)

        flat_patches = []
        for i in range(sampled_patches.shape[0]):
            sample_patch = sampled_patches[i].flatten()
            sample_patch = sample_patch - np.mean(sample_patch)
            sample_patch = sample_patch / np.linalg.norm(sample_patch)
            flat_patches.append(sample_patch)

        flat_patches = np.array(flat_patches)
        flat_patches = np.nan_to_num(flat_patches)
        normalised_patches = np.vstack([normalised_patches, flat_patches])

    return normalised_patches

# given sample labels, the number of images, and number of clusters, produce histograms of the sample label frequencies.
def get_vocab_histograms(sample_labels, num_images, num_clusters):
    vocab_histograms = []
    patches_per_image = sample_number

    for image in range(num_images):
        detected_labels = sample_labels[image * patches_per_image:(image + 1) * patches_per_image]
        histogram = np.zeros(num_clusters)
        for i in detected_labels:
            histogram[i] = histogram[i] + 1
        vocab_histograms.append(histogram)

    return np.array(vocab_histograms)

folders = [os.path.join(train_root_folder, x) for x in folders]
all_images = [img for folder in folders for img in load_images_from_folder(folder)]
all_images = np.array(all_images)
window_shape = (window_shape_length, window_shape_length)

normalised_train_patches = get_normalised_patches(all_images, sample_seperation, window_shape, sample_number)

print("Performing Clustering...")

kmeans = MiniBatchKMeans(n_clusters=num_clusters, batch_size=batch_size)
kmeans.fit(normalised_train_patches)

# for all patches, get their indices
patch_labels = kmeans.predict(normalised_train_patches)
num_train_images = all_images.shape[0]

train_vocab_histograms = get_vocab_histograms(patch_labels, num_train_images, num_clusters)

# histograms for each image obtained, y is our target for train_vocab_histograms above
y = []
for c in range(len(classes)):
    y.extend(np.full(training_images_per_class, c))
y = np.array(y)

clf = LinearSVC(tol=1e-5, multi_class='ovr')
clf.fit(train_vocab_histograms, y)

# run the model on labeled data and console output the accuracy
if(test_labeled_data):
    # model trained, read a test image, obtain its histogram, and predict it using clf
    folders = [os.path.join(labeled_test_root_folder, x) for x in test_images]
    test_images = [img for folder in folders for img in load_images_from_folder(folder)]
    test_images = np.array(test_images)

    num_test_images = test_images.shape[0]

    # to store normalised patches of the test images
    normalised_test_patches = get_normalised_patches(test_images, sample_seperation, window_shape, sample_number)
    patch_labels = kmeans.predict(normalised_test_patches)
    test_vocab_histograms = get_vocab_histograms(patch_labels, num_test_images, num_clusters)
    prediction = clf.predict(test_vocab_histograms)

    target = []
    for t in range(len(classes)):
        target.extend(np.full(math.floor(num_test_images/len(classes)), t))
    target = np.array(target)

    score = 0
    for i in range(prediction.shape[0]):
        if prediction[i] == target[i]:
            score = score + 1

    print("accuracy is: ")
    print(score/num_test_images)

if test_unlabeled_data == True:
    f = open(output_file, "w+")
    for image_file in os.listdir(unlabeled_test_root_folder):
        img = mpimg.imread(os.path.join(unlabeled_test_root_folder, image_file))
        if img is not None:
            if (resize_images == True):
                img = resize(img, (resize_scale, resize_scale), anti_aliasing=True)

        # classify the image
        normalised_patches = get_normalised_patches(np.array([img]), sample_seperation, window_shape, sample_number)
        patch_labels = kmeans.predict(normalised_patches)
        vocab_histogram = get_vocab_histograms(patch_labels, 1, num_clusters)
        prediction = clf.predict(vocab_histogram)

        # write prediction to text file
        f.write("%s " % image_file)
        f.write("%s\n" % classes[prediction[0]])
    f.close()